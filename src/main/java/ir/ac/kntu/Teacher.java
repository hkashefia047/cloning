package ir.ac.kntu;

public class Teacher implements Cloneable {
    private String name;
    private Course course;
    private Book favouriteBook;

    public Teacher(String name, Course course, Book favouriteBook) {
        this.name = name;
        this.course = course;
        this.favouriteBook = favouriteBook;
    }

    public Book getFavouriteBook() {
        return favouriteBook;
    }

    public void setFavouriteBook(Book favouriteBook) {
        this.favouriteBook = favouriteBook;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", course=" + course +
                ", favouriteBook=" + favouriteBook +
                '}';
    }

    @Override
    protected Teacher clone() throws CloneNotSupportedException {
     Course course=(Course) this.course.clone();
     Book book=(Book)this.favouriteBook.clone();
     Teacher teacher=(Teacher)super.clone();
     teacher.setCourse(course);
     teacher.setFavouriteBook(book);
     return teacher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
