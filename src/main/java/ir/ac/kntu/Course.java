package ir.ac.kntu;

public class Course implements Cloneable {
    private String subject;

    //this is method returns a shallow copy
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
